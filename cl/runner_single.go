package cl

type runner_single struct {
	runner_base
	ep IEP
}

func NewRunnerSingle(ep IEP) IRunner {
	return runner_single{ep: wrap_ep(ep)}
}

func (self runner_single) Schema() EPSchema {
	return self.ep.Schema()
}

func (self runner_single) Execute(prog string, args any) error {
	return self.ep.Execute(prog, args)
}

func (self runner_single) Run() {
	prog, args := self.get_env()
	if len(args) > 0 && self.is_help_arg(args[0]) {
		self.print_ep_help(prog, self.ep)
		return
	}
	self.run_ep(self.ep, prog, args)
}
