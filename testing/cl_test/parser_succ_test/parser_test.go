package parser_succ_test

import (
	"testing"

	"github.com/stretchr/testify/assert"
	"gitlab.com/asvedr/cldi/cl"
)

type Simple struct {
	ArgA string `short:"a" descr:"arg 1"`
	ArgB int    `long:"bb" descr:"arg 2"`
}

type DefaultParam struct {
	Arg int `short:"a" descr:"dflt" default:"1"`
}

type ShortAndLong struct {
	Arg int `short:"a" long:"abc"`
}

type Flag struct {
	Flag bool `short:"f" flag:"t" descr:"bool flag"`
}

type Multi struct {
	Vals []int `short:"m" descr:"int list" multi:"t"`
}

type Opt struct {
	OVal *int `short:"o" opt:"t" descr:"opt int"`
}

type Rest struct {
	X    int      `short:"x" default:"1"`
	RVal []string `rest:"t"`
	Y    string   `short:"y" default:"y"`
}

type PosKeyRest struct {
	X    int   `short:"x" default:"1"`
	RVal []int `rest:"t"`
	Y    int
}

type PosDefault struct {
	V int `default:"1"`
}

type Pair struct {
	A string
	B int
}

type CustomParser struct {
	Field Pair `short:"f"`
}

func (p *CustomParser) ParseField(args []string) error {
	p.Field = Pair{A: args[0], B: len(args[0])}
	return nil
}

type Choice struct {
	F string `short:"f"`
}

func (*Choice) ChoiceF() []string {
	return []string{"a", "b", "c"}
}

func TestSimple(t *testing.T) {
	parser := cl.MakeParser[Simple]()
	parsed, err := parser.Parse([]string{"-a", "x", "--bb", "2"})
	assert.Nil(t, err)
	req, casted := parsed.(Simple)
	assert.True(t, casted)
	assert.Equal(t, req, Simple{ArgA: "x", ArgB: 2})

	got := parser.Help()
	exp := "-a ArgA (string): arg 1\n--bb ArgB (int): arg 2"
	assert.Equal(t, got, exp)
}

func TestParseDefault(t *testing.T) {
	parser := cl.MakeParser[DefaultParam]()
	req, err := parser.Parse([]string{})
	assert.Nil(t, err)
	assert.Equal(t, req, DefaultParam{Arg: 1})

	got := parser.Help()
	exp := "-a Arg (int): dflt (default=1)"
	assert.Equal(t, got, exp)
}

func TestShortAndLong(t *testing.T) {
	parser := cl.MakeParser[ShortAndLong]()

	parsed, err := parser.Parse([]string{"-a", "3"})
	assert.Nil(t, err)
	req := parsed.(ShortAndLong)
	assert.Equal(t, req, ShortAndLong{Arg: 3})

	parsed, err = parser.Parse([]string{"--abc", "4"})
	assert.Nil(t, err)
	req = parsed.(ShortAndLong)
	assert.Equal(t, req, ShortAndLong{Arg: 4})

	got := parser.Help()
	exp := "-a --abc Arg (int)"
	assert.Equal(t, got, exp)
}

func TestBoolFlag(t *testing.T) {
	parser := cl.MakeParser[Flag]()

	parsed, err := parser.Parse([]string{})
	assert.Nil(t, err)
	assert.Equal(t, parsed.(Flag), Flag{Flag: false})

	parsed, err = parser.Parse([]string{"-f"})
	assert.Nil(t, err)
	assert.Equal(t, parsed.(Flag), Flag{Flag: true})

	got := parser.Help()
	exp := "-f Flag (flag): bool flag"
	assert.Equal(t, got, exp)
}

func TestMulti(t *testing.T) {
	parser := cl.MakeParser[Multi]()

	parsed, err := parser.Parse([]string{})
	assert.Nil(t, err)
	assert.Equal(t, len(parsed.(Multi).Vals), 0)

	parsed, err = parser.Parse([]string{"-m", "1", "-m", "2"})
	assert.Nil(t, err)
	assert.Equal(t, []int{1, 2}, parsed.(Multi).Vals)

	got := parser.Help()
	exp := "-m Vals ([]int): int list"
	assert.Equal(t, got, exp)
}

func TestOpt(t *testing.T) {
	parser := cl.MakeParser[Opt]()

	parsed, err := parser.Parse([]string{})
	assert.Nil(t, err)
	assert.Equal(t, parsed.(Opt), Opt{OVal: nil})

	parsed, err = parser.Parse([]string{"-o", "12"})
	assert.Nil(t, err)
	x12 := 12
	assert.Equal(t, parsed.(Opt), Opt{OVal: &x12})

	got := parser.Help()
	exp := "-o [OVal] (int): opt int"
	assert.Equal(t, got, exp)
}

func TestRest(t *testing.T) {
	parser := cl.MakeParser[Rest]()

	val, err := parser.Parse([]string{})
	assert.Nil(t, err)
	assert.Equal(t, val.(Rest), Rest{X: 1, Y: "y"})

	val, err = parser.Parse([]string{"a", "b"})
	assert.Nil(t, err)
	assert.Equal(t, val.(Rest), Rest{X: 1, Y: "y", RVal: []string{"a", "b"}})

	val, err = parser.Parse([]string{"-x", "2", "a", "-y", "3", "b"})
	assert.Nil(t, err)
	assert.Equal(t, val.(Rest), Rest{X: 2, Y: "3", RVal: []string{"a", "b"}})

	got := parser.Help()
	exp := "RVal* ([]string)\n-x X (int) (default=1)\n-y Y (string) (default=y)"
	assert.Equal(t, exp, got)
}

func TestPosKeyRest(t *testing.T) {
	parser := cl.MakeParser[PosKeyRest]()

	val, err := parser.Parse([]string{"2"})
	assert.Nil(t, err)
	assert.Equal(t, val.(PosKeyRest), PosKeyRest{X: 1, Y: 2})

	val, err = parser.Parse([]string{"2", "3"})
	assert.Nil(t, err)
	assert.Equal(t, val.(PosKeyRest), PosKeyRest{X: 1, Y: 2, RVal: []int{3}})

	got := parser.Help()
	exp := "Y (int)\nRVal* ([]int)\n-x X (int) (default=1)"
	assert.Equal(t, got, exp)
}

func TestPosDefault(t *testing.T) {
	parser := cl.MakeParser[PosDefault]()

	val, err := parser.Parse([]string{})
	assert.Nil(t, err)
	assert.Equal(t, val, PosDefault{V: 1})

	val, err = parser.Parse([]string{"2"})
	assert.Nil(t, err)
	assert.Equal(t, val, PosDefault{V: 2})
}

func TestCustomParser(t *testing.T) {
	parser := cl.MakeParser[CustomParser]()

	val, err := parser.Parse([]string{"-f", "x"})
	assert.Nil(t, err)
	assert.Equal(t, val, CustomParser{Field: Pair{A: "x", B: 1}})

	got := parser.Help()
	exp := "-f Field (parser_succ_test.Pair)"
	assert.Equal(t, exp, got)
}

func TestChoice(t *testing.T) {
	parser := cl.MakeParser[Choice]()

	val, err := parser.Parse([]string{"-f", "b"})
	assert.Nil(t, err)
	assert.Equal(t, val.(Choice), Choice{F: "b"})

	got := parser.Help()
	exp := "-f F {a,b,c}"
	assert.Equal(t, exp, got)
}
