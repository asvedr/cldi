package parser_panic_test

import (
	"testing"

	"github.com/stretchr/testify/assert"
	"gitlab.com/asvedr/cldi/cl"
)

type MultipleRest struct {
	A []int `rest:"t"`
	B []int `rest:"t"`
}

func TestMultipleRest(t *testing.T) {
	assert.Panics(t, func() { cl.MakeParser[MultipleRest]() })
}

type KeyCrossShort struct {
	A int `short:"x" long:"a"`
	B int `short:"x" long:"b"`
}

type KeyCrossLong struct {
	A int `short:"a" long:"x"`
	B int `short:"b" long:"x"`
}

func TestKeyCross(t *testing.T) {
	assert.Panics(
		t, func() { cl.MakeParser[KeyCrossShort]() },
	)
	assert.Panics(
		t, func() { cl.MakeParser[KeyCrossLong]() },
	)
}

type NoStdParser struct {
	A map[string]int `short:"a"`
}

func TestNoStdParser(t *testing.T) {
	assert.Panics(t, func() { cl.MakeParser[NoStdParser]() })
}

type InvalidCustomParserMethod struct {
	A int
}

func (*InvalidCustomParserMethod) ParseA(a int, b []string) error {
	return nil
}

func TestInvalidCustomParserMethod(t *testing.T) {
	assert.Panics(t, func() { cl.MakeParser[InvalidCustomParserMethod]() })
}

type InvalidChoiceMethod struct {
	A int
}

func (*InvalidChoiceMethod) ChoiceA() []int {
	return []int{1, 2, 3}
}

func TestInvalidChoiceMethod(t *testing.T) {
	assert.Panics(t, func() { cl.MakeParser[InvalidChoiceMethod]() })
}

type MultiNotASlice struct {
	M int `short:"m" multi:"t"`
}

func TestMultiNotASlice(t *testing.T) {
	assert.Panics(t, func() { cl.MakeParser[MultiNotASlice]() })
}

type RestNotASlice struct {
	M int `rest:"t"`
}

func TestRestNotASlice(t *testing.T) {
	assert.Panics(t, func() { cl.MakeParser[RestNotASlice]() })
}
