# CLDI: Command Line & Dependency Injection lib for go

## cldi/di Tools for dependency injection
It allows you to use singletons and factories to build app init system
```go
var Config = di.Singleton[*project.Config]{
	ErrFunc: func() (*project.Config, error) {
		return project.BuildConfig()
	},
}

var Client = di.Singleton[project.IClient]{
	PureFunc: func() project.IClient {
		return project.NewClient(Config.Get())
	},
}

var Repo = di.Factory[project.Repo] {
    PureFunc: func() project.Repo {
        return project.NewRepo(Client.Get())
    }
}
```

## cldi/cl Tools for command line interface and env var config
It generates CLI parser and help messages for your app

### Command line interface
```go
// declare entrypoint
type entrypoint struct {
	...
}

// declare CLI params
type request struct {
	Type string `short:"t" descr:"alert type" default:"notif"`
	Name string `descr:"alert name"`
	Msg  string `default:"" descr:"alert message"`
}

// declare available variants for "Type" field
func (request) ChoiceType() []string {
	s_map := genum.MakeFromString[ps2types.AlertType]()
	return giter.MKeys(s_map)
}

func New(...) cl.IEP {
	return ep{...}
}

// declare CLI schema for the entrypoint (required for IEP)
func (ep) Schema() cl.EPSchema {
	return cl.EPSchema{
        // command if required
		Command:     "force-alert",
        // description
		Description: "force alert",
        // command line parser
		ParamParser: cl.MakeParser[request](),
	}
}

// declare execute method for the entrypoint (required for IEP)
func (self ep) Execute(prog string, args any) error {
	...
}

func main() {
    cl.NewRunnerMulty(
        "",
        "program description",
        entrypoint{...},
        entrypoint2,
        entrypoint3,
    ).Run()
}
```
### Env config
```go
type Config struct {
	// Declare string var
	Abc  string `descr:"str val"`
	// Declare int var
	Def  int    `default:"2"`
	// Declare var prefix
	Meta byte   `prefix:"CNF_"`
}

/*
ENV:
	CNF_ABC=txtval
	CNF_DEF=33
*/
func main() {
	cnf, err := cl.BuildConfig[Config]()
	// result:
	cnf == &Config {Abc: "txtval", Def: 33}
	// print help if required
	println(cl.GetConfigHelp[Config]())
}
```