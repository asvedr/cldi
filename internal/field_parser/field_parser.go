package field_parser

import (
	"fmt"
	"reflect"
	"strings"

	"gitlab.com/asvedr/cldi/internal/consts"
	"gitlab.com/asvedr/cldi/internal/parser"
	"gitlab.com/asvedr/cldi/internal/validators"
)

type FieldSpec struct {
	Name         string
	Alias        string
	Short        string
	Long         string
	Descr        string
	Default      *string
	Multi        bool
	IsPositional bool
	IsFlag       bool
	IsRest       bool
	IsOpt        bool
	StrType      string
}

func (self *FieldSpec) MatchKey(key string) bool {
	if !strings.HasPrefix(key, "-") {
		return false
	}
	return self.Short == key || self.Long == key
}

type Parser[T any] struct {
	Spec   *FieldSpec
	Parse  func(*T, []string) error
	Choice func(*T) []string
}

func New[T any](
	st reflect.Type,
	field reflect.StructField,
	parser_set parser.Set,
) Parser[T] {
	spec := make_spec(field)
	parse := reveal_func[T](st, spec, parser_set)
	choice := reveal_choice[T](st, spec)
	return Parser[T]{Spec: spec, Parse: parse, Choice: choice}
}

func (self *Parser[T]) get_choice_slice() []string {
	var t T
	return self.Choice(&t)
}

func (self *Parser[T]) get_choice_set() map[string]struct{} {
	var t T
	set := map[string]struct{}{}
	for _, val := range self.Choice(&t) {
		set[val] = struct{}{}
	}
	return set
}

func (self *Parser[T]) CheckOutOfChoice(args []string) error {
	choice := self.get_choice_set()
	for _, arg := range args {
		_, found := choice[arg]
		if found {
			continue
		}
		return fmt.Errorf(
			"arg %s: %s is out of choice {%s}",
			self.Spec.Name,
			arg,
			strings.Join(self.get_choice_slice(), ","),
		)
	}
	return nil
}

func (self *Parser[T]) Help() string {
	var tokens []string
	if self.Spec.Short != "" {
		tokens = append(tokens, self.Spec.Short)
	}
	if self.Spec.Long != "" {
		tokens = append(tokens, self.Spec.Long)
	}
	if self.Spec.IsRest {
		tokens = append(tokens, self.Spec.Name+"*")
	} else if self.Spec.IsOpt {
		tokens = append(tokens, "["+self.Spec.Name+"]")
	} else {
		tokens = append(tokens, self.Spec.Name)
	}
	if self.Choice != nil {
		ch := strings.Join(self.get_choice_slice(), ",")
		tokens = append(tokens, "{"+ch+"}")
	} else if self.Spec.IsFlag {
		tokens = append(tokens, "(flag)")
	} else {
		tokens = append(tokens, "("+self.Spec.StrType+")")
	}
	if self.Spec.Descr != "" {
		tokens[len(tokens)-1] += ": " + self.Spec.Descr
	}
	if self.Spec.Default != nil {
		tokens = append(tokens, "(default="+*self.Spec.Default+")")
	}
	return strings.Join(tokens, " ")
}

func make_spec(field reflect.StructField) *FieldSpec {
	alias := field.Tag.Get(consts.TagName)
	if alias == "" {
		alias = field.Name
	}
	is_opt := reveal_bool(field.Tag.Get(consts.TagOpt))
	str_type := field.Type.String()
	if is_opt && strings.HasPrefix(str_type, "*") {
		str_type = strings.TrimPrefix(str_type, "*")
	}
	short := field.Tag.Get(consts.TagShort)
	if short != "" {
		short = "-" + short
	}
	long := field.Tag.Get(consts.TagLong)
	if long != "" {
		long = "--" + long
	}
	is_positional := short == "" && long == ""
	is_rest := reveal_bool(field.Tag.Get(consts.TagRest))
	var default_ *string
	dflt_val, dflt_found := field.Tag.Lookup(consts.TagDefault)
	if dflt_found {
		default_ = &dflt_val
	}
	return &FieldSpec{
		Name:         field.Name,
		Alias:        alias,
		Short:        short,
		Long:         long,
		Descr:        field.Tag.Get(consts.TagDescr),
		Default:      default_,
		Multi:        is_rest || reveal_bool(field.Tag.Get(consts.TagMulti)),
		IsFlag:       reveal_bool(field.Tag.Get(consts.TagFlag)),
		IsRest:       is_rest,
		IsOpt:        is_opt,
		StrType:      str_type,
		IsPositional: is_positional,
	}
}

func reveal_bool(src string) bool {
	if src == "" {
		return false
	}
	val, found := consts.BoolValues[src]
	if !found {
		panic("invalid bool value: " + src)
	}
	return val
}

func reveal_func[T any](
	st reflect.Type,
	spec *FieldSpec,
	parser_set parser.Set,
) func(*T, []string) error {
	ptr_st := reflect.PointerTo(st)
	parse_method, found := ptr_st.MethodByName("Parse" + spec.Name)
	if found {
		return reveal_custom_method[T](st, parse_method)
	} else {
		return reveal_default_method[T](st, spec, parser_set)
	}
}

func reveal_choice[T any](
	st reflect.Type,
	spec *FieldSpec,
) func(*T) []string {
	ptr_st := reflect.PointerTo(st)
	method, found := ptr_st.MethodByName("Choice" + spec.Name)
	if !found {
		return nil
	}
	// return reveal_choice_method(st, spec, choice_method)
	validators.ChoiceMethodType(
		st.Name(),
		method.Name,
		method.Type.String(),
	)
	as_int := method.Func.Interface()
	as_func := as_int.(func(*T) []string)
	return as_func
}

func reveal_custom_method[T any](
	st reflect.Type,
	method reflect.Method,
) func(*T, []string) error {
	validators.ParseMethodType(
		st.Name(),
		method.Name,
		method.Type.String(),
	)
	as_int := method.Func.Interface()
	as_func := as_int.(func(*T, []string) error)
	return as_func
}

func reveal_default_method[T any](
	st reflect.Type,
	spec *FieldSpec,
	parser_set parser.Set,
) func(*T, []string) error {
	field, _ := st.FieldByName(spec.Name)
	tp := field.Type.String()
	var parser func([]string) (reflect.Value, error)
	if spec.IsRest || spec.Multi {
		parser = reveal_slice_parser(st, spec, tp, parser_set)
	} else {
		elt_parser := reveal_single_parser(tp, parser_set)
		parser = func(s []string) (reflect.Value, error) {
			return elt_parser(s[0])
		}
	}
	return make_assign_method[T](spec, parser)
}

func make_assign_method[T any](
	spec *FieldSpec,
	parser func([]string) (reflect.Value, error),
) func(*T, []string) error {
	return func(req *T, args []string) error {
		val, err := parser(args)
		if err != nil {
			return err
		}
		elem := reflect.ValueOf(req).Elem()
		field := elem.FieldByName(spec.Name)
		field.Set(val)
		return nil
	}
}

func reveal_slice_parser(
	st reflect.Type,
	spec *FieldSpec,
	tp string,
	parser_set parser.Set,
) func([]string) (reflect.Value, error) {
	if !strings.HasPrefix(tp, "[]") {
		msg := fmt.Sprintf(
			"field %s.%s is not slice", st.Name(), spec.Name,
		)
		panic(msg)
	}
	pure_tp := strings.TrimPrefix(tp, "[]")
	parse_elt := reveal_single_parser(pure_tp, parser_set)
	field, _ := st.FieldByName(spec.Name)
	return func(args []string) (reflect.Value, error) {
		acc := reflect.MakeSlice(field.Type, 0, 0)
		for _, arg := range args {
			val, err := parse_elt(arg)
			if err != nil {
				return reflect.Value{}, err
			}
			acc = reflect.Append(acc, val)
		}
		return acc, nil
	}
}

func reveal_single_parser(
	tp string,
	parser_set parser.Set,
) func(string) (reflect.Value, error) {
	fun := parser_set.GetOpt(tp)
	if fun != nil {
		return fun
	}
	return parser.StdParserSet.Get().Get(tp)
}
