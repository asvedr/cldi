package cl

type IParser interface {
	Parse([]string) (any, error)
	Help() string
}

type IUrlQueryParser interface {
	Parse(map[string][]string) (any, error)
	Help() string
}

type IEP interface {
	Schema() EPSchema
	Execute(prog string, args any) error
}

type IRunner interface {
	IEP
	Run()
}
