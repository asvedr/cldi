package parser

import (
	"errors"
	"reflect"
	"strconv"

	"gitlab.com/asvedr/cldi/di"
	"gitlab.com/asvedr/cldi/internal/consts"
)

type Set struct {
	parsers map[string]Parser
}

var StdParserSet = di.Singleton[Set]{PureFunc: create_std_table}

func NewSet() Set {
	return Set{parsers: map[string]Parser{}}
}

func (self Set) GetOpt(tp string) Parser {
	return self.parsers[tp]
}

func (self Set) Get(tp string) Parser {
	fun := self.parsers[tp]
	if fun == nil {
		panic("can not reveal std parser for " + tp)
	}
	return fun
}

func AddParser[T any](set Set, f func(string) (T, error)) {
	var t T
	var ptr_t *T
	set.parsers[reflect.TypeOf(t).String()] = convert_fun[T](f)
	ptr_f := func(src string) (*T, error) {
		if src == "" {
			return nil, nil
		}
		val, err := f(src)
		return &val, err
	}
	set.parsers[reflect.TypeOf(ptr_t).String()] = convert_fun[*T](ptr_f)
}

var err_invalid_bool = errors.New("invalid bool value")

func create_std_table() Set {
	res := NewSet()
	AddParser(res, strconv.Atoi)
	ifun := func(s string) (int64, error) {
		return strconv.ParseInt(s, 10, 64)
	}
	AddParser(res, ifun)
	sfun := func(s string) (string, error) {
		return s, nil
	}
	AddParser(res, sfun)
	bfun := func(s string) (bool, error) {
		val, found := consts.BoolValues[s]
		if !found {
			return val, err_invalid_bool
		}
		return val, nil
	}
	AddParser(res, bfun)
	ffun := func(s string) (float64, error) {
		return strconv.ParseFloat(s, 64)
	}
	AddParser(res, ffun)
	return res
}

func convert_fun[T any](f func(string) (T, error)) Parser {
	return func(src string) (reflect.Value, error) {
		val, err := f(src)
		return reflect.ValueOf(val), err
	}
}
