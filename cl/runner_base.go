package cl

import (
	"errors"
	"fmt"
	"os"
)

type runner_base struct{}

func (runner_base) get_env() (string, []string) {
	args := os.Args
	return args[0], args[1:]
}

func (self runner_base) run_ep(
	ep IEP,
	prog string,
	args []string,
) {
	schema := ep.Schema()
	var parsed any
	var err error
	if schema.ParamParser == nil {
		if len(args) > 0 {
			err = errors.New(prog + " does not expect args")
		}
	} else {
		parsed, err = schema.ParamParser.Parse(args)
	}
	self.exit_on_err(err)
	self.exit_on_err(ep.Execute(prog, parsed))
}

func (runner_base) exit_on_err(err error) {
	if err == nil {
		return
	}
	os.Stderr.WriteString(err.Error() + "\n")
	os.Exit(1)
}

func (runner_base) print_ep_help(prog string, ep IEP) {
	schema := ep.Schema()
	var params string
	if schema.ParamParser != nil {
		params = "\n" + schema.ParamParser.Help() + "\n"
	}
	fmt.Printf(
		"%s - %s%s",
		prog,
		schema.Description,
		params,
	)
}

func (runner_base) is_help_arg(arg string) bool {
	return arg == "-h" || arg == "--help"
}
