package query_paraser

import (
	"fmt"
	"reflect"
	"strings"

	"gitlab.com/asvedr/cldi/internal/field_parser"
	"gitlab.com/asvedr/cldi/internal/parser"
)

type Parser[T any] struct {
	fields         map[string]field_parser.Parser[T]
	fields_as_list []field_parser.Parser[T]
}

func New[T any](parser_set parser.Set) *Parser[T] {
	var req T
	st := reflect.TypeOf(req)
	fields := map[string]field_parser.Parser[T]{}
	fields_as_list := []field_parser.Parser[T]{}

	for i := 0; i < st.NumField(); i += 1 {
		fld := st.Field(i)
		fprs := field_parser.New[T](st, fld, parser_set)
		fields[fprs.Spec.Alias] = fprs
		fields_as_list = append(fields_as_list, fprs)
	}

	return &Parser[T]{
		fields:         fields,
		fields_as_list: fields_as_list,
	}
}

func (self Parser[T]) Parse(src map[string][]string) (any, error) {
	var instance T
	for name, field := range self.fields {
		field_args := src[name]
		err := self.validate_args(field, field_args)
		if err != nil {
			return nil, err
		}
		if len(field_args) > 0 {
			err = field.Parse(&instance, src[name])
			if err != nil {
				return nil, err
			}
		}
	}
	return instance, nil
}

func (self *Parser[T]) validate_args(
	fprs field_parser.Parser[T],
	args []string,
) error {
	if len(args) > 1 && !fprs.Spec.Multi {
		return fmt.Errorf("arg %s expect only one value", fprs.Spec.Name)
	}
	if len(args) == 0 {
		if !fprs.Spec.IsOpt && !fprs.Spec.Multi {
			return fmt.Errorf("arg %s not set", fprs.Spec.Name)
		}
	}
	if fprs.Choice != nil {
		return fprs.CheckOutOfChoice(args)
	}
	return nil
}

func (self *Parser[T]) Help() string {
	var lines []string
	for _, field := range self.fields_as_list {
		lines = append(lines, field.Help())
	}
	return strings.Join(lines, "\n")
}
