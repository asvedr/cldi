package parser

import (
	"reflect"
)

type Parser func(string) (reflect.Value, error)
