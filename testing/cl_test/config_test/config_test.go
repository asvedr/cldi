package config_test

import (
	"os"
	"testing"

	"github.com/stretchr/testify/assert"
	"gitlab.com/asvedr/cldi/cl"
)

type Config struct {
	Abc  string `descr:"str val"`
	Def  int    `default:"2"`
	Meta byte   `prefix:"CNF_"`
}

var succ_vars = []map[string]string{
	{"CNF_ABC": "x", "CNF_DEF": "1"},
	{"cnf_Abc": "y"},
}
var succ_resps = []*Config{
	{Abc: "x", Def: 1},
	{Abc: "y", Def: 2},
}

func TestConfig(t *testing.T) {
	for i, vars := range succ_vars {
		for k, v := range vars {
			assert.Nil(t, os.Setenv(k, v))
		}
		expected := succ_resps[i]
		got, err := cl.BuildConfig[Config]()
		assert.Nil(t, err)
		assert.Equal(t, expected, got)
		for k := range vars {
			assert.Nil(t, os.Unsetenv(k))
		}
	}
}

func TestForceVars(t *testing.T) {
	v := map[string]string{"abc": "xyz", "def": "123"}
	got, err := cl.BuildConfigWithVars[Config](v)
	assert.Nil(t, err)
	exp := &Config{Abc: "xyz", Def: 123}
	assert.Equal(t, exp, got)
}

func TestHelp(t *testing.T) {
	help := cl.GetConfigHelp[Config]()
	exp := "CNF_ABC: str val\nCNF_DEF (default=2)\nparse err: field Abc: var not set"
	assert.Equal(t, exp, help)
}
