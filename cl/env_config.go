package cl

import "reflect"

func BuildConfig[T any]() (*T, error) {
	return BuildConfigWithVars[T](map[string]string{})
}

func BuildConfigWithVars[T any](force map[string]string) (*T, error) {
	var result T
	st := reflect.TypeOf(result)

	builder := new_config_builder[T](st)
	err := builder.build(&result, force)
	return &result, err
}

func GetConfigHelp[T any]() string {
	var t T
	st := reflect.TypeOf(t)
	builder := new_config_builder[T](st)
	return builder.gen_help()
}

func DescribeConfig[T any](config T) string {
	st := reflect.TypeOf(config)
	builder := new_config_builder[T](st)
	return builder.describe(reflect.ValueOf(config))
}
