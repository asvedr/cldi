package di

import "sync"

type IPureMaker[Args, Res any] interface {
	Make(Args) Res
}

type IMaker[Args, Res any] interface {
	Make(Args) (Res, error)
}

type MockPureMaker[Args, Res any] struct {
	mu sync.Mutex
	Calls   []Args
	Results []Res
}

type MockMaker[Args, Res any] struct {
	mu sync.Mutex
	Calls   []Args
	Results []Result[Res]
}

type Result[T any] struct {
	Val T
	Err error
}

func (self *MockPureMaker[Args, Res]) Make(args Args) Res {
	self.mu.Lock()
	defer self.mu.Unlock()
	self.Calls = append(self.Calls, args)
	res := self.Results[0]
	self.Results = self.Results[1:]
	return res
}

func (self *MockMaker[Args, Res]) Make(args Args) (Res, error) {
	self.mu.Lock()
	defer self.mu.Unlock()
	self.Calls = append(self.Calls, args)
	res := self.Results[0]
	self.Results = self.Results[1:]
	return res.Val, res.Err
}
