package validators

import (
	"fmt"
	"strings"
)

func ParseMethodType(
	st string,
	method string,
	// Expected: func(*str-name, []string) error
	tp string,
) {
	if !strings.HasPrefix(tp, "func(") {
		panic(fmt.Sprintf("method %s.%s has invalid type: %s", st, method, tp))
	}
	split := strings.SplitN(tp, "(", 2)
	split = strings.SplitN(split[1], ")", 2)
	args_got := strings.SplitN(split[0], ", ", 2)[1]
	args_expected := "[]string"
	if args_got != args_expected {
		msg := fmt.Sprintf(
			"method %s.%s must expect args []string",
			st,
			method,
		)
		panic(msg)
	}
	res_got := strings.TrimSpace(split[1])
	res_expected := "error"
	if res_got != res_expected {
		msg := fmt.Sprintf(
			"method %s.%s must have return type error",
			st,
			method,
		)
		panic(msg)
	}
}

func ChoiceMethodType(
	st string,
	method string,
	// Expected: func(*str-name) []string
	tp string,
) {
	if !strings.HasPrefix(tp, "func(") {
		panic(fmt.Sprintf("method %s.%s has invalid type: %s", st, method, tp))
	}
	split := strings.SplitN(tp, "(", 2)
	split = strings.SplitN(split[1], ")", 2)
	if strings.Contains(split[0], ",") {
		panic(fmt.Sprintf("method %s.%s must not expect args", st, method))
	}
	res_got := strings.TrimSpace(split[1])
	res_expected := "[]string"
	if res_got != res_expected {
		panic(fmt.Sprintf("method %s.%s must have return type []string", st, method))
	}
}
