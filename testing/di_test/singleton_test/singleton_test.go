package singleton_test

import (
	"errors"
	"testing"
	"time"

	"github.com/stretchr/testify/assert"
	"gitlab.com/asvedr/cldi/di"
)

func TestSimpleSingleton(t *testing.T) {
	var single = di.Singleton[time.Time]{PureFunc: time.Now}
	a := single.Get()
	time.Sleep(time.Millisecond)
	b := single.Get()
	assert.Equal(t, a, b)
}

func TestSingletonResOk(t *testing.T) {
	var single = di.Singleton[time.Time]{
		ErrFunc: func() (time.Time, error) {
			return time.Now(), nil
		},
	}
	a := single.Get()
	time.Sleep(time.Millisecond)
	b := single.Get()
	assert.Equal(t, a, b)
}

func TestSingletonResErr(t *testing.T) {
	var single = di.Singleton[time.Time]{
		ErrFunc: func() (time.Time, error) {
			return time.Now(), errors.New("oops")
		},
	}
	assert.Panics(t, func() { single.Get() })
}
