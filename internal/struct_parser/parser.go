package struct_parser

import (
	"fmt"
	"reflect"
	"strings"

	"gitlab.com/asvedr/cldi/internal/field_parser"
	"gitlab.com/asvedr/cldi/internal/parser"
)

type Parser[T any] struct {
	key_params []field_parser.Parser[T]
	pos_params []field_parser.Parser[T]
	rest_param *field_parser.Parser[T]
}

func New[T any](parser_set parser.Set) *Parser[T] {
	var req T
	st := reflect.TypeOf(req)

	short_keys := map[string]struct{}{}
	long_keys := map[string]struct{}{}

	var with_keys []field_parser.Parser[T]
	var positional []field_parser.Parser[T]
	var rest []field_parser.Parser[T]
	var rest_names []string

	for i := 0; i < st.NumField(); i += 1 {
		fld := st.Field(i)
		fprs := field_parser.New[T](st, fld, parser_set)
		if fprs.Spec.IsRest {
			rest = append(rest, fprs)
			rest_names = append(rest_names, fprs.Spec.Name)
		} else if fprs.Spec.IsPositional {
			positional = append(positional, fprs)
		} else {
			with_keys = append(with_keys, fprs)
			if fprs.Spec.Short != "" {
				_, found := short_keys[fprs.Spec.Short]
				if found {
					panic(st.Name() + " duplicate short key: " + fprs.Spec.Short)
				}
				short_keys[fprs.Spec.Short] = struct{}{}
			}
			if fprs.Spec.Long != "" {
				_, found := long_keys[fprs.Spec.Long]
				if found {
					panic(st.Name() + " duplicate long key: " + fprs.Spec.Short)
				}
				long_keys[fprs.Spec.Long] = struct{}{}
			}
		}
	}

	if len(rest) > 1 {
		panic("multiple rest fields: " + strings.Join(rest_names, ","))
	}

	var single_rest *field_parser.Parser[T]
	if len(rest) == 1 {
		single_rest = &rest[0]
	}

	return &Parser[T]{
		key_params: with_keys,
		pos_params: positional,
		rest_param: single_rest,
	}
}

func (self *Parser[T]) Parse(args []string) (any, error) {
	var instance T
	used_fields := map[int]bool{}
	for _, fprs := range self.parsers_in_parse_order() {
		field_args, err := self.collect_args(fprs.Spec, args, used_fields)
		if err != nil {
			return nil, err
		}
		err = self.validate_args(fprs, field_args)
		if err != nil {
			return nil, err
		}
		if len(field_args) > 0 {
			err = fprs.Parse(&instance, field_args)
		}
		if err != nil {
			return nil, fmt.Errorf("arg %s: %v", fprs.Spec.Name, err)
		}
	}
	var garbage []string
	for i, arg := range args {
		if !used_fields[i] {
			garbage = append(garbage, arg)
		}
	}
	if len(garbage) > 0 {
		return nil, fmt.Errorf("unused args: %v", garbage)
	}
	return instance, nil
}

func (self *Parser[T]) Help() string {
	var lines []string
	for _, field := range self.parsers_in_help_order() {
		lines = append(lines, field.Help())
	}
	return strings.Join(lines, "\n")
}

func (self *Parser[T]) parsers_in_parse_order() []field_parser.Parser[T] {
	parsers := append([]field_parser.Parser[T]{}, self.key_params...)
	parsers = append(parsers, self.pos_params...)
	if self.rest_param != nil {
		parsers = append(parsers, *self.rest_param)
	}
	return parsers
}

func (self *Parser[T]) parsers_in_help_order() []field_parser.Parser[T] {
	parsers := append([]field_parser.Parser[T]{}, self.pos_params...)
	if self.rest_param != nil {
		parsers = append(parsers, *self.rest_param)
	}
	return append(parsers, self.key_params...)
}

func (self *Parser[T]) collect_args(
	spec *field_parser.FieldSpec,
	args []string,
	used map[int]bool,
) ([]string, error) {
	var collected []string
	var err error
	if spec.IsRest {
		collected = self.collect_rest(args, used)
	} else if spec.IsPositional {
		collected = self.collect_positional(args, used)
	} else {
		collected, err = self.collect_by_keys(spec, args, used)
	}
	if err != nil {
		return nil, err
	}
	if len(collected) > 0 {
		return collected, nil
	}
	if spec.Default != nil {
		return []string{*spec.Default}, nil
	}
	return []string{}, nil
}

func (self *Parser[T]) validate_args(
	fprs field_parser.Parser[T],
	args []string,
) error {
	if len(args) > 1 && !fprs.Spec.Multi {
		return fmt.Errorf("arg %s expect only one value", fprs.Spec.Name)
	}
	if len(args) == 0 {
		if !fprs.Spec.IsOpt && !fprs.Spec.Multi {
			return fmt.Errorf("arg %s not set", fprs.Spec.Name)
		}
	}
	if fprs.Choice != nil {
		return fprs.CheckOutOfChoice(args)
	}
	return nil
}

func (self *Parser[T]) collect_positional(
	args []string,
	used map[int]bool,
) []string {
	for i, arg := range args {
		if used[i] {
			continue
		}
		used[i] = true
		return []string{arg}
	}
	return []string{}
}

func (self *Parser[T]) collect_by_keys(
	spec *field_parser.FieldSpec,
	args []string,
	used map[int]bool,
) ([]string, error) {
	var result []string
	for i, arg := range args {
		if used[i] {
			continue
		}
		if !spec.MatchKey(arg) {
			continue
		}
		used[i] = true
		if spec.IsFlag {
			return []string{"t"}, nil
		}
		if i+1 >= len(args) {
			return nil, fmt.Errorf("key " + arg + " has no value")
		}
		used[i+1] = true
		result = append(result, args[i+1])
	}
	if len(result) == 0 {
		if spec.IsFlag {
			return []string{"f"}, nil
		}
	}
	return result, nil
}

func (self *Parser[T]) collect_rest(args []string, used map[int]bool) []string {
	var result []string
	for i, arg := range args {
		if !used[i] {
			result = append(result, arg)
			used[i] = true
		}
	}
	return result
}
