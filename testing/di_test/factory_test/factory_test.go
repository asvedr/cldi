package factory_test

import (
	"errors"
	"testing"
	"time"

	"github.com/stretchr/testify/assert"
	"gitlab.com/asvedr/cldi/di"
)

func TestSimpleFactory(t *testing.T) {
	var factory = di.Factory[time.Time]{PureFunc: time.Now}
	a := factory.Get()
	time.Sleep(time.Millisecond)
	b := factory.Get()
	assert.Less(t, a, b)
}

func TestFactoryResOk(t *testing.T) {
	var factory = di.Factory[time.Time]{
		ErrFunc: func() (time.Time, error) {
			return time.Now(), nil
		},
	}
	a := factory.Get()
	time.Sleep(time.Millisecond)
	b := factory.Get()
	assert.Less(t, a, b)
}

func TestFactoryResErr(t *testing.T) {
	var factory = di.Factory[time.Time]{
		ErrFunc: func() (time.Time, error) {
			return time.Now(), errors.New("oops")
		},
	}
	assert.Panics(t, func() { factory.Get() })
}
