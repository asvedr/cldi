package cl

import (
	"errors"
	"fmt"
	"os"
	"reflect"
	"strings"

	"gitlab.com/asvedr/cldi/internal/consts"
	"gitlab.com/asvedr/cldi/internal/field_parser"
	"gitlab.com/asvedr/cldi/internal/parser"
)

type config_builder[T any] struct {
	prefix  string
	parsers []field_parser.Parser[T]
}

func new_config_builder[T any](st reflect.Type) config_builder[T] {
	pset := parser.NewSet()
	self := config_builder[T]{}
	for i := 0; i < st.NumField(); i += 1 {
		fld := st.Field(i)
		if fld.Name == consts.MetaField {
			self.process_meta_field(fld)
			continue
		}
		prs := field_parser.New[T](st, fld, pset)
		self.parsers = append(self.parsers, prs)
	}
	return self
}

func (self *config_builder[T]) describe(config reflect.Value) string {
	var lines []string
	for _, field := range self.parsers {
		key := self.prepare_key(field.Spec.Alias)
		value := config.FieldByName(field.Spec.Name).Interface()
		lines = append(lines, fmt.Sprintf("%s=%v", key, value))
	}
	return strings.Join(lines, "\n")
}

func (self *config_builder[T]) gen_help() string {
	var lines []string
	for _, field := range self.parsers {
		lines = append(lines, self.get_field_help(field))
	}
	lines = append(lines, self.get_parse_status())
	return strings.Join(lines, "\n")
}

func (self *config_builder[T]) get_field_help(p field_parser.Parser[T]) string {
	res := self.prepare_key(p.Spec.Alias)
	if p.Spec.Descr != "" {
		res += ": " + p.Spec.Descr
	}
	if p.Spec.Default != nil {
		res += " (default=" + *p.Spec.Default + ")"
	}
	return res
}

func (self *config_builder[T]) get_parse_status() string {
	var t T
	err := self.build(&t, map[string]string{})
	if err != nil {
		return "parse err: " + err.Error()
	}
	return "parsed"
}

func (self *config_builder[T]) process_meta_field(fld reflect.StructField) {
	self.prefix = strings.ToUpper(fld.Tag.Get(consts.TagPrefix))
}

func (self *config_builder[T]) build(
	result *T,
	force map[string]string,
) error {
	environ := self.get_environ()
	for key, val := range force {
		environ[self.prepare_key(key)] = val
	}
	var err error
	for _, prs := range self.parsers {
		name := prs.Spec.Alias
		val, found := environ[self.prepare_key(name)]
		if found {
			err = prs.Parse(result, []string{val})
		} else if prs.Spec.Default != nil {
			err = prs.Parse(result, []string{*prs.Spec.Default})
		} else {
			err = errors.New("var not set")
		}
		if err != nil {
			return fmt.Errorf("field %s: %v", name, err)
		}
	}
	return nil
}

func (self *config_builder[T]) prepare_key(key string) string {
	return self.prefix + strings.ToUpper(key)
}

func (self *config_builder[T]) get_environ() map[string]string {
	result := map[string]string{}
	for _, src := range os.Environ() {
		split := strings.SplitN(src, "=", 2)
		result[strings.ToUpper(split[0])] = split[1]
	}
	return result
}
