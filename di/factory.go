package di

import "errors"

type Factory[T any] struct {
	Name     string
	PureFunc func() T
	ErrFunc  func() (T, error)
}

func (self *Factory[T]) Get() T {
	if self.PureFunc != nil {
		return self.PureFunc()
	}
	if self.ErrFunc != nil {
		val, err := self.ErrFunc()
		if err != nil {
			panic(prepare_msg(err, self.Name))
		}
		return val
	}
	err := errors.New("PureFunc == nil AND ErrFunc == nil")
	panic(prepare_msg(err, self.Name))
}
