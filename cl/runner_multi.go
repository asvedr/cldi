package cl

import (
	"errors"
	"fmt"
)

type runner_multi struct {
	runner_base
	command     string
	description string
	eps         []IEP
}

func NewRunnerMulty(
	command string,
	description string,
	eps ...IEP,
) IRunner {
	var wrapped []IEP
	for _, ep := range eps {
		wrapped = append(wrapped, wrap_ep(ep))
	}
	return runner_multi{
		command:     command,
		description: description,
		eps:         wrapped,
	}
}

func (self runner_multi) Run() {
	prog, args := self.get_env()
	self.exec(prog, args)
}

func (self runner_multi) Execute(prog string, args any) error {
	self.exec(prog, args.([]string))
	return nil
}

func (self runner_multi) exec(prog string, args []string) {
	if len(args) == 0 || self.is_help_arg(args[0]) {
		self.print_runner_help(prog)
		return
	}
	ep := self.choose_ep(args[0])
	if ep == nil {
		self.exit_on_err(errors.New("invalid option: " + args[0]))
	}
	prog += " " + args[0]
	args = args[1:]
	if len(args) > 0 && self.is_help_arg(args[0]) {
		self.print_ep_help(prog, ep)
		return
	}
	self.run_ep(ep, prog, args)
}

func (self runner_multi) Schema() EPSchema {
	return EPSchema{
		Command:     self.command,
		Description: self.description,
		ParamParser: runner_param_parser{eps: self.eps},
	}
}

func (self runner_multi) choose_ep(cmd string) IEP {
	for _, ep := range self.eps {
		if ep.Schema().Command == cmd {
			return ep
		}
	}
	return nil
}

func (self runner_multi) print_runner_help(prog string) {
	var params string
	for _, ep := range self.eps {
		schema := ep.Schema()
		params += "\n  " + schema.Command + ": " + schema.Description
	}
	fmt.Printf("%s - %s%s\n", prog, self.description, params)
}
