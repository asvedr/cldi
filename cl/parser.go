package cl

import (
	"gitlab.com/asvedr/cldi/internal/parser"
	"gitlab.com/asvedr/cldi/internal/query_paraser"
	"gitlab.com/asvedr/cldi/internal/struct_parser"
)

var empty_set = parser.NewSet()

type ParserContext struct {
	set parser.Set
}

func MakeParser[T any](ctx ...ParserContext) IParser {
	if len(ctx) == 0 {
		return struct_parser.New[T](empty_set)
	} else {
		return struct_parser.New[T](ctx[0].set)
	}
}

func MakeQueryParser[T any](ctx ...ParserContext) IUrlQueryParser {
	if len(ctx) == 0 {
		return query_paraser.New[T](empty_set)
	} else {
		return query_paraser.New[T](ctx[0].set)
	}
}

func NewCtx() ParserContext {
	return ParserContext{set: parser.NewSet()}
}

func AddType[T any](ctx ParserContext, f func(string) (T, error)) {
	parser.AddParser(ctx.set, f)
}
