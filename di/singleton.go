package di

import (
	"errors"
	"sync"
)

type Singleton[T any] struct {
	once     sync.Once
	value    T
	Name     string
	PureFunc func() T
	ErrFunc  func() (T, error)
}

func (self *Singleton[T]) Get() T {
	self.once.Do(self.calculate)
	return self.value
}

func (self *Singleton[T]) calculate() {
	if self.PureFunc != nil {
		self.value = self.PureFunc()
		return
	}
	if self.ErrFunc != nil {
		val, err := self.ErrFunc()
		if err != nil {
			panic(prepare_msg(err, self.Name))
		}
		self.value = val
		return
	}
	err := errors.New("PureFunc == nil AND ErrFunc == nil")
	panic(prepare_msg(err, self.Name))
}
