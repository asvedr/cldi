package bug_force_alert_test

import (
	"testing"

	"github.com/stretchr/testify/assert"
	"gitlab.com/asvedr/cldi/cl"
)

type request struct {
	Type string `short:"t" descr:"alert type"`
	Name string `descr:"alert name"`
	Msg  string `default:"" descr:"alert message"`
}

func (request) ChoiceType() []string {
	return []string{"notif", "ok", "fail"}
}

func TestForceAlert(t *testing.T) {
	parser := cl.MakeParser[request]()
	parsed, err := parser.Parse([]string{"a", "-t", "ok"})
	assert.Nil(t, err)
	assert.Equal(
		t,
		parsed.(request),
		request{
			Type: "ok",
			Name: "a",
			Msg:  "",
		},
	)
}
