package consts

const TagShort = "short"
const TagLong = "long"
const TagDescr = "descr"
const TagDefault = "default"
const TagOpt = "opt"
const TagMulti = "multi"
const TagFlag = "flag"
const TagRest = "rest"
const TagName = "name"

const MetaField = "Meta"
const TagPrefix = "prefix"

var BoolValues = map[string]bool{
	"true":  true,
	"t":     true,
	"yes":   true,
	"1":     true,
	"false": false,
	"f":     false,
	"no":    false,
	"0":     false,
}
