package cl

import "strings"

type runner_param_parser struct {
	eps []IEP
}

func (self runner_param_parser) Parse(args []string) (any, error) {
	return args, nil
}

func (self runner_param_parser) Help() string {
	var lines []string
	for _, ep := range self.eps {
		schema := ep.Schema()
		lines = append(
			lines,
			"  "+schema.Command+": "+schema.Description,
		)
	}
	return strings.Join(lines, "\n")
}
