package di

func prepare_msg(err error, name string) string {
	if len(name) == 0 {
		return err.Error()
	}
	return name + ": " + err.Error()
}
