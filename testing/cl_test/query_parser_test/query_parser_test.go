package query_parser_test

import (
	"errors"
	"strings"
	"testing"

	"github.com/stretchr/testify/assert"
	"gitlab.com/asvedr/cldi/cl"
)

type Request struct {
	OptInt *int     `name:"opt_int" opt:"t"`
	Int    int      `name:"int"`
	SList  []string `name:"s_list" multi:"t"`
}

func TestQueryParser(t *testing.T) {
	prs := cl.MakeQueryParser[Request]()
	val, err := prs.Parse(map[string][]string{
		"opt_int": {},
		"int":     {"2"},
		"s_list":  {"a", "bc"},
	})
	assert.Nil(t, err)
	assert.Nil(t, val.(Request).OptInt)
	assert.Equal(t, val.(Request).Int, 2)
	assert.Equal(t, val.(Request).SList, []string{"a", "bc"})
}

type Enum int

type CustomTypeRequest struct {
	Fld Enum `name:"fld"`
}

func parse_enum(src string) (Enum, error) {
	switch src {
	case "a":
		return 1, nil
	case "b":
		return 2, nil
	default:
		return 0, errors.New("err")
	}
}

func TestCustomTypeRequest(t *testing.T) {
	ctx := cl.NewCtx()
	cl.AddType(ctx, parse_enum)
	prs := cl.MakeQueryParser[CustomTypeRequest](ctx)
	val, err := prs.Parse(map[string][]string{"fld": {"b"}})
	assert.Nil(t, err)
	assert.Equal(t, val.(CustomTypeRequest).Fld, Enum(2))
}

func TestHelp(t *testing.T) {
	prs := cl.MakeQueryParser[Request]()
	help := []string{
		"[OptInt] (int)",
		"Int (int)",
		"SList ([]string)",
	}
	assert.Equal(t, strings.Join(help, "\n"), prs.Help())
}
