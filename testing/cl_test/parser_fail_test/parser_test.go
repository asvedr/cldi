package parser_fail_test

import (
	"errors"
	"testing"

	"github.com/stretchr/testify/assert"
	"gitlab.com/asvedr/cldi/cl"
)

type StdParams struct {
	PosInt int
	F64    float64 `short:"f"`
}

type Choice struct {
	A string
}

func (*Choice) ChoiceA() []string {
	return []string{"a", "b", "c"}
}

func (self *Choice) ParseA(val []string) error {
	if val[0] == "b" {
		return errors.New("custom error")
	}
	self.A = val[0]
	return nil
}

func TestArgNotSet(t *testing.T) {
	parser := cl.MakeParser[StdParams]()

	_, err := parser.Parse([]string{"-f", "1.2"})
	assert.Equal(t, err.Error(), "arg PosInt not set")

	_, err = parser.Parse([]string{})
	assert.Equal(t, err.Error(), "arg F64 not set")
}

func TestParserFailed(t *testing.T) {
	parser := cl.MakeParser[StdParams]()

	_, err := parser.Parse([]string{"-f", "abc"})
	assert.Equal(t, err.Error(), `arg F64: strconv.ParseFloat: parsing "abc": invalid syntax`)
}

func TestMultipleArgsForSingle(t *testing.T) {
	parser := cl.MakeParser[StdParams]()

	_, err := parser.Parse([]string{"1", "-f", "2", "-f", "3"})
	assert.Equal(t, err.Error(), "arg F64 expect only one value")
}

func TestKeyWithNoValue(t *testing.T) {
	parser := cl.MakeParser[StdParams]()

	_, err := parser.Parse([]string{"1", "-f"})
	assert.Equal(t, err.Error(), "key -f has no value")
}

func TestGarbageData(t *testing.T) {
	parser := cl.MakeParser[StdParams]()

	_, err := parser.Parse([]string{"1", "-f", "2.0", "a", "b"})
	assert.Equal(t, err.Error(), "unused args: [a b]")
}

func TestOutOfChoice(t *testing.T) {
	parser := cl.MakeParser[Choice]()

	_, err := parser.Parse([]string{"1"})
	assert.Equal(t, err.Error(), "arg A: 1 is out of choice {a,b,c}")
}

func TestCustomError(t *testing.T) {
	parser := cl.MakeParser[Choice]()

	_, err := parser.Parse([]string{"b"})
	assert.Equal(t, err.Error(), "arg A: custom error")
}
