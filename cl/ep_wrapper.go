package cl

type ep_wrapper struct {
	ep    IEP
	cache *EPSchema
}

func wrap_ep(ep IEP) IEP {
	if _, casted := ep.(runner_multi); casted {
		return ep
	}

	if _, casted := ep.(runner_single); casted {
		return ep
	}
	return &ep_wrapper{ep: ep}
}

func (self *ep_wrapper) Schema() EPSchema {
	if self.cache == nil {
		val := self.ep.Schema()
		self.cache = &val
	}
	return *self.cache
}

func (self *ep_wrapper) Execute(prog string, args any) error {
	return self.ep.Execute(prog, args)
}
